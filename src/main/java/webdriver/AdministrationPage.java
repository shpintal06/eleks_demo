package webdriver;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.sonatype.inject.Parameters;

public class AdministrationPage {
    private static final String URL = "http://eds_university.eleks.com/administration/products/7";
    private WebDriver driver;

    @FindBy(how = How.XPATH, using = "//div[@class=\"form-group\"]/child::a")
    public WebElement addNewProductButton;

    @FindBy(how = How.XPATH, using = "//div[@class=\"form-group read-only\"]/child::input")
    public WebElement productNameField;

    @FindBy(how = How.XPATH, using = "//div[@class=\"dropdown\"]/child::button")
    public WebElement productFamilyDropDown;

    @FindBy(how = How.XPATH, using = "//div[@class=\"dropdown\"]/child::ul/child::li[position()=5]")
    public WebElement productFamily;

    @FindBy(how = How.XPATH, using = "//*[@id=\"toast-container\"]/div/div/child::span")
    private WebElement message;

    @FindBy(how = How.XPATH, using = "//button[@class='btn gds-btn gds-btn-success']")
    public WebElement saveNewProductButton;

    @FindBy(how = How.XPATH, using = "//div[@class='validation-message validation-message__name']")
    public WebElement nameIsPeqired;

    @FindBy(how = How.XPATH, using = "//div[@class='validation-message']")
    public WebElement productFamilyNameIsRequired;

    @FindBy(how = How.XPATH, using = "/html/body/app/main/administration/div/div/div/projects/div/div/div/div/search-field/div/input")
    public WebElement searchProductField;

    @FindBy(how = How.XPATH, using = "//div[@class='col-md-12 section-body preview-list__wrapper ps-container ps-theme-default']/ul/li/a")
    public WebElement chooseCreatedProduct;

    @FindBy(how = How.XPATH, using = "//div[@class='section-title__details-actions']/button[@class='btn gds-btn-icon gds-edit-icon']")
    public WebElement editButton;
    public AdministrationPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
        WebDriverWait wait = new WebDriverWait(driver, 20);
        wait.until(ExpectedConditions.urlToBe(URL));
    }

    @FindBy(how = How.XPATH, using = "//button[@class='btn gds-btn gds-btn-success']")
    public WebElement saveAfterEditButton;

    @FindBy(how = How.XPATH, using = "//button[@class='btn gds-btn-icon gds-delete-icon']")
    public WebElement deleteButton;

    @FindBy(how = How.XPATH, using = "//button[@class='btn gds-btn gds-ml-1 gds-btn-danger']")
    public  WebElement finalDeleteButton;

    public boolean isActive() {
        return driver.getCurrentUrl().equals(URL);
    }
    @Parameters

    public void createNewProduct(){
        addNewProductButton.click();
    }

    public void fillAllFields(String words) {
        fillInProductName(words);
        clickDropDowndProductFamily();
        chooseProductFamily();
        fillInDescription(words);
        fillInRepository(words);
    }

    public void fillInDescription(String text) {
        Actions actions = new Actions(driver);
        driver.switchTo().frame(0);
        WebElement element = driver.findElement(By.xpath("//html/body/p"));
        actions.moveToElement(element);
        actions.click();
        actions.sendKeys(text);
        actions.build().perform();
        driver.switchTo().parentFrame();
    }

    public void fillInRepository(String text) {
        Actions actions = new Actions(driver);
        driver.switchTo().frame(1);
        WebElement element = driver.findElement(By.xpath("//html/body/p"));
        actions.moveToElement(element);
        actions.click();
        actions.sendKeys(text);
        actions.build().perform();
        driver.switchTo().parentFrame();
    }

    public void fillInProductName(String words){
        productNameField.clear();
        productNameField.sendKeys(words);
    }

    public void clickDropDowndProductFamily(){
        productFamilyDropDown.click();
    }

    public void chooseProductFamily(){
        productFamily.click();
    }

    public void handleSaveButtonClick(){
        saveNewProductButton.click();
    }

    public String saveNewProduct(){
        handleSaveButtonClick();

        WebDriverWait wait = new WebDriverWait(driver, 15);
        wait.until(ExpectedConditions.visibilityOf(message));
        return message.getText();
    }

    public boolean isSaved() {
        return saveNewProduct().contains("successfully created");
    }

    public boolean isEdited() {
        return saveNewProduct().contains("successfully updated");
    }


    public boolean requiredFieldsEmpty() {
        handleSaveButtonClick();
        WebDriverWait wait = new WebDriverWait(driver, 5);

        try {
            wait.until(ExpectedConditions.visibilityOf(nameIsPeqired));
            wait.until(ExpectedConditions.visibilityOf(productFamilyNameIsRequired));
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public void searchProduct(String words){
        searchProductField.clear();
        searchProductField.sendKeys(words);
    }

    public void chooseCreatedProduct() throws InterruptedException {
        Thread.sleep(200);
        chooseCreatedProduct.click();
    }

    public void editCreatedProduct(String editedWords){
        editButton.click();


        fillInDescription(editedWords);
        fillInRepository(editedWords);
    }

    public void saveAfterEdition(){
        saveAfterEditButton.click();
    }

    public void deleteCreatedProduct(){
        deleteButton.click();
    }

    public boolean finalDelete() {
        WebDriverWait wait = new WebDriverWait(driver, 10);
        try {
            wait.until(ExpectedConditions.visibilityOf(finalDeleteButton));
            finalDeleteButton.click();
            return  true;
        } catch (Exception e) {
            return false;

        }
    }
}
