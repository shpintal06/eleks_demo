package webdriver;

import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Issue;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;

public class AdministrationPageTest {
    private final String URL = "http://eds_university.eleks.com/login";
    private AdministrationPage administrationPage;
    private WebDriver driver;

    @Parameters({"username", "password"})
    @BeforeMethod
    public void logIn(String username, String password){
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\juliaa_sha\\Downloads\\chromedriver_win32\\chromedriver.exe");
        driver = DriverFactory.getDriver("Chrome");

        driver.get(URL);
        LogInPage logInPage = new LogInPage(driver);

        SingInPage singInPage = logInPage.openSingInPage();
        singInPage.fillInForm(username, password);

        MainPage mainPage = singInPage.openMainPage();
        administrationPage = mainPage.openAdministrationPage();
    }

    @AfterMethod
    public void closeDriver(){
        driver.close();
    }

    @Parameters({"words"})
    @Issue("UI-2")
    @Feature("UI functionality")
    @Description("Test verifies that product can be created and all product attributes are correctly recorded")
    @Test
    public void testCreatesProduct(String words) {
        administrationPage.createNewProduct();
        administrationPage.fillAllFields(words);
        Assert.assertTrue(administrationPage.isSaved());
    }
    @Issue("UI-2")
    @Feature("UI functionality")
    @Description("Test verifies that product can`t submit when required attributes are not filled in")
    @Test
    public void testCreatesProductWithoutProductName() throws IOException {
        administrationPage.createNewProduct();
        Assert.assertTrue(administrationPage.requiredFieldsEmpty());

        if(!administrationPage.requiredFieldsEmpty()) {

            TakesScreenshot screenshot = ((TakesScreenshot) driver);
            File scrFile = screenshot.getScreenshotAs(OutputType.FILE);
            File destFile = new File("screenshot.png");
            FileUtils.copyFile(scrFile, destFile);
        }

    }

    @Parameters({"editedWords", "words"})
    @Description("Test verifies that product can be edited")
    @Test
    public void testdEditesCreatedProduct(String editedWords, String words) throws InterruptedException {
        administrationPage.searchProduct(words);

        administrationPage.chooseCreatedProduct();
        administrationPage.editCreatedProduct(editedWords);
        administrationPage.saveAfterEdition();
        Assert.assertTrue(administrationPage.isEdited());
    }

    @Parameters({"words"})
    @Description("Test verifies that product can be deleted")
    @Test
    public void testeDeletesCreatedProduct(String words) throws InterruptedException {
        administrationPage.searchProduct(words);

        administrationPage.chooseCreatedProduct();
        administrationPage.deleteCreatedProduct();

        Assert.assertTrue(administrationPage.finalDelete());

    }

}
