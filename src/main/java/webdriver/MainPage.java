package webdriver;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class MainPage {

    private static final String URL = "http://eds_university.eleks.com/modules/2/products";

    private WebDriver driver;

    @FindBy(how = How.XPATH, using = "//div[@id='navbar']/child::ul/child::li[position()=2]/a")
    public WebElement administrationButton;

//    @FindBy(xpath = "//*div[@id='navbar']/child::ul/child::li[@class='dropdown']/child::a/child::span")
//    private WebElement userDropDown;
//
//    @FindBy(xpath = "//*div[@id='navbar']/child::ul/child::li[@class='dropdown open']/child::ul/child::li[@role='menuitem']/child::a")
//    private WebElement logOutButton;

    public MainPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.urlToBe(URL));
    }

    public boolean isActive() {
        return driver.getCurrentUrl().equals(URL);
    }

    public AdministrationPage openAdministrationPage(){
        administrationButton.click();
        return new AdministrationPage(driver);
    }

//    public void logOut(){
//        userDropDown.click();
//        logOutButton.click();
//        setActive(false);
//    }
}
