package webdriver;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;

public class DriverFactory {
    public static WebDriver getDriver(String name) {
        switch (name) {
            case "Chrome":
                return new ChromeDriver();
            case "Edge":
                return new EdgeDriver();
            default:
                return new ChromeDriver();
        }
    }
}
