package webdriver;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class SingInPage {
    private final WebDriver driver;

    @FindBy(how = How.XPATH, using = "//*[@class='col-xs-12']/child::button")
    public WebElement singInButton;

    @FindBy(how = How.XPATH, using = "//input[@id='email']")
    public WebElement singInEmail;

    @FindBy(how = How.XPATH, using = "//input[@id='userPassword']")
    public WebElement singInPassword;

    public SingInPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void fillInForm(String email, String password) {
        singInEmail.clear();
        singInEmail.sendKeys(email);

        singInPassword.clear();
        singInPassword.sendKeys(password);
    }

    public MainPage openMainPage(){
        singInButton.click();
        return new MainPage(driver);
    }
}
