package webdriver;

import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Issue;
import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.*;


public class LogInPageTest {

    private final String URL = "http://eds_university.eleks.com/login";
    private WebDriver driver;

    @BeforeMethod
    public void startDriver() {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\juliaa_sha\\Downloads\\chromedriver_win32\\chromedriver.exe");
        driver = DriverFactory.getDriver("Chrome");
//        WebDriverManager.edgedriver().setup();
//        driver = DriverFactory.getDriver("Edge");
    }


    @AfterMethod
    public void closeDriver(){
        driver.close();
    }

    @Parameters({"username", "password"})
    @Test
    @Issue("UI-1")
    @Feature("UI functionality")
    @Description("Test verifies that user with valid credentials can login into site")
    @Step
    public void testUsernameAndPasswordAreValid(String username, String password) {
        driver.get(URL);
        LogInPage logInPage = new LogInPage(driver);
        SingInPage singInPage = logInPage.openSingInPage();

        singInPage.fillInForm(username, password);
        MainPage mainPage = singInPage.openMainPage();

        Assert.assertTrue(mainPage.isActive());
    }

    @Parameters({"password", "invalidUsername"})
    @Test
    @Issue("UI-2")
    @Feature("UI functionality")
    @Description("Test verifies that user with invalid username and valid password cannot login into site")
    @Step
    public void testUsernameIsInvalidAndPasswordIsValid(String password, String invalidUsername){
        driver.get(URL);
        LogInPage logInPage = new LogInPage(driver);
        SingInPage singInPage = logInPage.openSingInPage();

        singInPage.fillInForm(invalidUsername, password);
        Assert.assertEquals(driver.getCurrentUrl(), URL);
    }
    @Parameters({"username"})
    @Test
    @Issue("UI-3")
    @Feature("UI functionality")
    @Description("Test verifies that user with valid username and empty password cannot login into site")
    @Step
    public void testUsernameValidAndPasswordIsEmpty(String username){
        driver.get(URL);
        LogInPage logInPage = new LogInPage(driver);
        SingInPage singInPage = logInPage.openSingInPage();

        singInPage.fillInForm(username, "");
        Assert.assertEquals(driver.getCurrentUrl(), URL);
    }

    @Parameters({"username"})
    @Test
    @Issue("UI-3")
    @Feature("UI functionality")
    @Description("Test verifies that user with valid username and empty password cannot login into site")
    @Step
    public void testUsernameValidAndPasswordIsOversized(String username){
        driver.get(URL);
        LogInPage logInPage = new LogInPage(driver);
        SingInPage singInPage = logInPage.openSingInPage();

        String password = "";
        for (int i = 0; i < 300; i++) {
            password+="a";
        }

        singInPage.fillInForm(username, password);
        Assert.assertEquals(driver.getCurrentUrl(), URL);
    }

}
